variable "env" {
  type = string
}

variable "application" {
  type = string
  default = "blog-infra"
}

# --- provider --------
variable "region" {
  description = "The region used to launch this module resources."
  type = string
}

variable "profile" {
  description = "The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  type = string
  default = ""
}

# --- networking --------
variable "vpc_cidr" {
  description = "The CIDR block of the vpc"
  type = string
}

variable "public_subnets_cidr" {
  description = "The CIDR block for the public subnet"
  type = list(string)
}

variable "private_subnets_cidr" {
  description = "The CIDR block for the private subnet"
  type = list(string)
}

variable "availability_zones" {
  description = "The az that the resources will be launched"
  type = list(string)
}

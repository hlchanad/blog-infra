terraform {
  backend "remote" {
    organization = "hlchanad"

    workspaces {
      prefix = "blog-infra-"
    }
  }
}
application = "blog-infra"
env = "dev"

# provider
region = "ap-southeast-1"
profile = "default"

vpc_cidr = "10.0.0.0/16"
public_subnets_cidr = ["10.0.0.0/24"]
private_subnets_cidr = ["10.0.16.0/20"]
availability_zones = ["ap-southeast-1a"]
